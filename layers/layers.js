var wms_layers = [];


        var lyr_ESRITopo_0 = new ol.layer.Tile({
            'title': 'ESRI Topo',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}'
            })
        });

        var lyr_ESRIPhysical_1 = new ol.layer.Tile({
            'title': 'ESRI Physical',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Physical_Map/MapServer/tile/{z}/{y}/{x}'
            })
        });
var format_RUVTotal_2 = new ol.format.GeoJSON();
var features_RUVTotal_2 = format_RUVTotal_2.readFeatures(json_RUVTotal_2, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_RUVTotal_2 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_RUVTotal_2.addFeatures(features_RUVTotal_2);
var lyr_RUVTotal_2 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_RUVTotal_2, 
                style: style_RUVTotal_2,
                interactive: false,
    title: 'RUV Total<br />\
    <img src="styles/legend/RUVTotal_2_0.png" /> 1 - 500<br />\
    <img src="styles/legend/RUVTotal_2_1.png" /> 500 - 1000<br />\
    <img src="styles/legend/RUVTotal_2_2.png" /> 1000 - 2000<br />\
    <img src="styles/legend/RUVTotal_2_3.png" /> 2000 - 5000<br />\
    <img src="styles/legend/RUVTotal_2_4.png" /> 5000 - 10000<br />\
    <img src="styles/legend/RUVTotal_2_5.png" /> 10000 - 100000<br />\
    <img src="styles/legend/RUVTotal_2_6.png" /> 100000 - 150000<br />\
    <img src="styles/legend/RUVTotal_2_7.png" /> 0.0 - 0.0<br />'
        });
var format_RUV20052008_3 = new ol.format.GeoJSON();
var features_RUV20052008_3 = format_RUV20052008_3.readFeatures(json_RUV20052008_3, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_RUV20052008_3 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_RUV20052008_3.addFeatures(features_RUV20052008_3);
var lyr_RUV20052008_3 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_RUV20052008_3, 
                style: style_RUV20052008_3,
                interactive: false,
    title: 'RUV 2005-2008<br />\
    <img src="styles/legend/RUV20052008_3_0.png" /> 0 - 500<br />\
    <img src="styles/legend/RUV20052008_3_1.png" /> 500 - 1000<br />\
    <img src="styles/legend/RUV20052008_3_2.png" /> 1000 - 10000<br />\
    <img src="styles/legend/RUV20052008_3_3.png" /> 10000 - 20000<br />\
    <img src="styles/legend/RUV20052008_3_4.png" /> 20000 - 50000<br />\
    <img src="styles/legend/RUV20052008_3_5.png" /> Más de 50 mil víctimas<br />'
        });
var format_RUV20002004_4 = new ol.format.GeoJSON();
var features_RUV20002004_4 = format_RUV20002004_4.readFeatures(json_RUV20002004_4, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_RUV20002004_4 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_RUV20002004_4.addFeatures(features_RUV20002004_4);
var lyr_RUV20002004_4 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_RUV20002004_4, 
                style: style_RUV20002004_4,
                interactive: false,
    title: 'RUV 2000-2004<br />\
    <img src="styles/legend/RUV20002004_4_0.png" /> Menos de 100 víctimas<br />\
    <img src="styles/legend/RUV20002004_4_1.png" /> 100 - 200<br />\
    <img src="styles/legend/RUV20002004_4_2.png" /> 200 - 500<br />\
    <img src="styles/legend/RUV20002004_4_3.png" /> 500 - 1000<br />\
    <img src="styles/legend/RUV20002004_4_4.png" /> 1000 - 10000<br />\
    <img src="styles/legend/RUV20002004_4_5.png" /> 10000 - 20000<br />\
    <img src="styles/legend/RUV20002004_4_6.png" /> 20000 - 50000<br />\
    <img src="styles/legend/RUV20002004_4_7.png" /> Más de 50 mil vítimas<br />'
        });
var format_RUV19951999_5 = new ol.format.GeoJSON();
var features_RUV19951999_5 = format_RUV19951999_5.readFeatures(json_RUV19951999_5, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_RUV19951999_5 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_RUV19951999_5.addFeatures(features_RUV19951999_5);
var lyr_RUV19951999_5 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_RUV19951999_5, 
                style: style_RUV19951999_5,
                interactive: false,
    title: 'RUV 1995-1999<br />\
    <img src="styles/legend/RUV19951999_5_0.png" /> Menos de 100 víctimas<br />\
    <img src="styles/legend/RUV19951999_5_1.png" /> 100 - 200<br />\
    <img src="styles/legend/RUV19951999_5_2.png" /> 200 - 500<br />\
    <img src="styles/legend/RUV19951999_5_3.png" /> 500 - 1000<br />\
    <img src="styles/legend/RUV19951999_5_4.png" /> 1000 - 5000<br />\
    <img src="styles/legend/RUV19951999_5_5.png" /> Más de 5 mil víctimas<br />'
        });
var format_RUV19901994_6 = new ol.format.GeoJSON();
var features_RUV19901994_6 = format_RUV19901994_6.readFeatures(json_RUV19901994_6, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_RUV19901994_6 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_RUV19901994_6.addFeatures(features_RUV19901994_6);
var lyr_RUV19901994_6 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_RUV19901994_6, 
                style: style_RUV19901994_6,
                interactive: false,
    title: 'RUV 1990-1994<br />\
    <img src="styles/legend/RUV19901994_6_0.png" /> Menos de 100 víctimas<br />\
    <img src="styles/legend/RUV19901994_6_1.png" /> 100 - 200<br />\
    <img src="styles/legend/RUV19901994_6_2.png" /> 200 - 500<br />\
    <img src="styles/legend/RUV19901994_6_3.png" /> 500 - 1000<br />\
    <img src="styles/legend/RUV19901994_6_4.png" /> 1000 - 5000<br />\
    <img src="styles/legend/RUV19901994_6_5.png" /> Más de 5 mil víctimas<br />'
        });
var format_RUV19851989_7 = new ol.format.GeoJSON();
var features_RUV19851989_7 = format_RUV19851989_7.readFeatures(json_RUV19851989_7, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_RUV19851989_7 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_RUV19851989_7.addFeatures(features_RUV19851989_7);
var lyr_RUV19851989_7 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_RUV19851989_7, 
                style: style_RUV19851989_7,
                interactive: false,
    title: 'RUV 1985-1989<br />\
    <img src="styles/legend/RUV19851989_7_0.png" /> Menos de 100 víctimas<br />\
    <img src="styles/legend/RUV19851989_7_1.png" /> 100 - 200<br />\
    <img src="styles/legend/RUV19851989_7_2.png" /> 200 - 500<br />\
    <img src="styles/legend/RUV19851989_7_3.png" /> 500 - 1000<br />\
    <img src="styles/legend/RUV19851989_7_4.png" /> 1000 - 5000<br />\
    <img src="styles/legend/RUV19851989_7_5.png" /> Más de 5 mil víctimas<br />'
        });
var format_IncursionesFARC_8 = new ol.format.GeoJSON();
var features_IncursionesFARC_8 = format_IncursionesFARC_8.readFeatures(json_IncursionesFARC_8, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_IncursionesFARC_8 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_IncursionesFARC_8.addFeatures(features_IncursionesFARC_8);
var lyr_IncursionesFARC_8 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_IncursionesFARC_8, 
                style: style_IncursionesFARC_8,
                interactive: false,
                title: '<img src="styles/legend/IncursionesFARC_8.png" /> Incursiones FARC'
            });
var format_1a4aConferencia_9 = new ol.format.GeoJSON();
var features_1a4aConferencia_9 = format_1a4aConferencia_9.readFeatures(json_1a4aConferencia_9, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_1a4aConferencia_9 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_1a4aConferencia_9.addFeatures(features_1a4aConferencia_9);
var lyr_1a4aConferencia_9 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_1a4aConferencia_9, 
                style: style_1a4aConferencia_9,
                interactive: false,
                title: '<img src="styles/legend/1a4aConferencia_9.png" /> 1a - 4a Conferencia'
            });
var format_5a6aConferencia_10 = new ol.format.GeoJSON();
var features_5a6aConferencia_10 = format_5a6aConferencia_10.readFeatures(json_5a6aConferencia_10, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_5a6aConferencia_10 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_5a6aConferencia_10.addFeatures(features_5a6aConferencia_10);
var lyr_5a6aConferencia_10 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_5a6aConferencia_10, 
                style: style_5a6aConferencia_10,
                interactive: false,
                title: '<img src="styles/legend/5a6aConferencia_10.png" /> 5a - 6a Conferencia'
            });
var format_7aConferencia_11 = new ol.format.GeoJSON();
var features_7aConferencia_11 = format_7aConferencia_11.readFeatures(json_7aConferencia_11, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_7aConferencia_11 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_7aConferencia_11.addFeatures(features_7aConferencia_11);
var lyr_7aConferencia_11 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_7aConferencia_11, 
                style: style_7aConferencia_11,
                interactive: false,
                title: '<img src="styles/legend/7aConferencia_11.png" /> 7a Conferencia'
            });
var format_8aConferencia_12 = new ol.format.GeoJSON();
var features_8aConferencia_12 = format_8aConferencia_12.readFeatures(json_8aConferencia_12, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_8aConferencia_12 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_8aConferencia_12.addFeatures(features_8aConferencia_12);
var lyr_8aConferencia_12 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_8aConferencia_12, 
                style: style_8aConferencia_12,
                interactive: false,
                title: '<img src="styles/legend/8aConferencia_12.png" /> 8a Conferencia'
            });
var format_9aConferencia_13 = new ol.format.GeoJSON();
var features_9aConferencia_13 = format_9aConferencia_13.readFeatures(json_9aConferencia_13, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_9aConferencia_13 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_9aConferencia_13.addFeatures(features_9aConferencia_13);
var lyr_9aConferencia_13 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_9aConferencia_13, 
                style: style_9aConferencia_13,
                interactive: false,
                title: '<img src="styles/legend/9aConferencia_13.png" /> 9a Conferencia'
            });
var format_10aConferencia_14 = new ol.format.GeoJSON();
var features_10aConferencia_14 = format_10aConferencia_14.readFeatures(json_10aConferencia_14, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_10aConferencia_14 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_10aConferencia_14.addFeatures(features_10aConferencia_14);
var lyr_10aConferencia_14 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_10aConferencia_14, 
                style: style_10aConferencia_14,
                interactive: false,
                title: '<img src="styles/legend/10aConferencia_14.png" /> 10a Conferencia'
            });
var format_Densidad_de_Coca_National_mod_15 = new ol.format.GeoJSON();
var features_Densidad_de_Coca_National_mod_15 = format_Densidad_de_Coca_National_mod_15.readFeatures(json_Densidad_de_Coca_National_mod_15, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Densidad_de_Coca_National_mod_15 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Densidad_de_Coca_National_mod_15.addFeatures(features_Densidad_de_Coca_National_mod_15);
var lyr_Densidad_de_Coca_National_mod_15 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Densidad_de_Coca_National_mod_15, 
                style: style_Densidad_de_Coca_National_mod_15,
                interactive: false,
                title: '<img src="styles/legend/Densidad_de_Coca_National_mod_15.png" /> Densidad_de_Coca_National_mod'
            });
var format_SalidaExilio_16 = new ol.format.GeoJSON();
var features_SalidaExilio_16 = format_SalidaExilio_16.readFeatures(json_SalidaExilio_16, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_SalidaExilio_16 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_SalidaExilio_16.addFeatures(features_SalidaExilio_16);
var lyr_SalidaExilio_16 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_SalidaExilio_16, 
                style: style_SalidaExilio_16,
                interactive: true,
                title: '<img src="styles/legend/SalidaExilio_16.png" /> Salida Exilio'
            });

        var lyr_ESRIReferenceOverlay_17 = new ol.layer.Tile({
            'title': 'ESRI Reference Overlay',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}'
            })
        });
var group_CONFERENCIASDELASFARC = new ol.layer.Group({
                                layers: [lyr_1a4aConferencia_9,lyr_5a6aConferencia_10,lyr_7aConferencia_11,lyr_8aConferencia_12,lyr_9aConferencia_13,lyr_10aConferencia_14,],
                                title: "CONFERENCIAS DE LAS FARC"});
var group_INCURSIONESDELASFARC = new ol.layer.Group({
                                layers: [lyr_IncursionesFARC_8,],
                                title: "INCURSIONES DE LAS FARC"});
var group_RegistronicodeVictimasRUV = new ol.layer.Group({
                                layers: [lyr_RUVTotal_2,lyr_RUV20052008_3,lyr_RUV20002004_4,lyr_RUV19951999_5,lyr_RUV19901994_6,lyr_RUV19851989_7,],
                                title: "Registro Único de Victimas -RUV-"});
var group_Basemap = new ol.layer.Group({
                                layers: [lyr_ESRITopo_0,lyr_ESRIPhysical_1,],
                                title: "Basemap"});

lyr_ESRITopo_0.setVisible(false);lyr_ESRIPhysical_1.setVisible(false);lyr_RUVTotal_2.setVisible(false);lyr_RUV20052008_3.setVisible(false);lyr_RUV20002004_4.setVisible(false);lyr_RUV19951999_5.setVisible(false);lyr_RUV19901994_6.setVisible(false);lyr_RUV19851989_7.setVisible(false);lyr_IncursionesFARC_8.setVisible(false);lyr_1a4aConferencia_9.setVisible(false);lyr_5a6aConferencia_10.setVisible(false);lyr_7aConferencia_11.setVisible(false);lyr_8aConferencia_12.setVisible(false);lyr_9aConferencia_13.setVisible(false);lyr_10aConferencia_14.setVisible(false);lyr_Densidad_de_Coca_National_mod_15.setVisible(false);lyr_SalidaExilio_16.setVisible(false);lyr_ESRIReferenceOverlay_17.setVisible(true);
var layersList = [group_Basemap,group_RegistronicodeVictimasRUV,group_INCURSIONESDELASFARC,group_CONFERENCIASDELASFARC,lyr_Densidad_de_Coca_National_mod_15,lyr_SalidaExilio_16,lyr_ESRIReferenceOverlay_17];
lyr_RUVTotal_2.set('fieldAliases', {'OBJECTID': 'OBJECTID', 'MunNombre': 'MunNombre', 'DANE': 'DANE', 'Shape_Leng': 'Shape_Leng', 'Shape_Area': 'Shape_Area', 'OBJECTID_1': 'OBJECTID_1', 'MunNombr_1': 'MunNombr_1', 'DANE_1': 'DANE_1', 'Date1_1_19': 'Date1_1_19', 'Date1_1_20': 'Date1_1_20', 'Date1_1_21': 'Date1_1_21', 'Date1_1_22': 'Date1_1_22', 'Date1_1_23': 'Date1_1_23', 'Date1_1_24': 'Date1_1_24', 'Date1_1_25': 'Date1_1_25', 'Date1_1_26': 'Date1_1_26', 'Date1_1_27': 'Date1_1_27', 'Date1_1_28': 'Date1_1_28', 'Date1_1_29': 'Date1_1_29', 'Date1_1_30': 'Date1_1_30', 'Date1_1_31': 'Date1_1_31', 'Date1_1_32': 'Date1_1_32', 'Date1_1_33': 'Date1_1_33', 'Date1_1_34': 'Date1_1_34', 'Date1_1_35': 'Date1_1_35', 'Date1_1_36': 'Date1_1_36', 'Date1_1_37': 'Date1_1_37', 'Date1_1_38': 'Date1_1_38', 'Date1_1_39': 'Date1_1_39', 'Date1_1_40': 'Date1_1_40', 'Date1_1_41': 'Date1_1_41', 'Date1_1_42': 'Date1_1_42', 'RUV85_89': 'RUV85_89', 'RUV90_94': 'RUV90_94', 'RUV95_99': 'RUV95_99', 'RUV00_04': 'RUV00_04', 'RUV05_08': 'RUV05_08', 'RUV_Total': 'RUV_Total', });
lyr_RUV20052008_3.set('fieldAliases', {'OBJECTID': 'OBJECTID', 'MunNombre': 'MunNombre', 'DANE': 'DANE', 'Shape_Leng': 'Shape_Leng', 'Shape_Area': 'Shape_Area', 'OBJECTID_1': 'OBJECTID_1', 'MunNombr_1': 'MunNombr_1', 'DANE_1': 'DANE_1', 'Date1_1_19': 'Date1_1_19', 'Date1_1_20': 'Date1_1_20', 'Date1_1_21': 'Date1_1_21', 'Date1_1_22': 'Date1_1_22', 'Date1_1_23': 'Date1_1_23', 'Date1_1_24': 'Date1_1_24', 'Date1_1_25': 'Date1_1_25', 'Date1_1_26': 'Date1_1_26', 'Date1_1_27': 'Date1_1_27', 'Date1_1_28': 'Date1_1_28', 'Date1_1_29': 'Date1_1_29', 'Date1_1_30': 'Date1_1_30', 'Date1_1_31': 'Date1_1_31', 'Date1_1_32': 'Date1_1_32', 'Date1_1_33': 'Date1_1_33', 'Date1_1_34': 'Date1_1_34', 'Date1_1_35': 'Date1_1_35', 'Date1_1_36': 'Date1_1_36', 'Date1_1_37': 'Date1_1_37', 'Date1_1_38': 'Date1_1_38', 'Date1_1_39': 'Date1_1_39', 'Date1_1_40': 'Date1_1_40', 'Date1_1_41': 'Date1_1_41', 'Date1_1_42': 'Date1_1_42', 'RUV85_89': 'RUV85_89', 'RUV90_94': 'RUV90_94', 'RUV95_99': 'RUV95_99', 'RUV00_04': 'RUV00_04', 'RUV05_08': 'RUV05_08', 'RUV_Total': 'RUV_Total', });
lyr_RUV20002004_4.set('fieldAliases', {'OBJECTID': 'OBJECTID', 'MunNombre': 'MunNombre', 'DANE': 'DANE', 'Shape_Leng': 'Shape_Leng', 'Shape_Area': 'Shape_Area', 'OBJECTID_1': 'OBJECTID_1', 'MunNombr_1': 'MunNombr_1', 'DANE_1': 'DANE_1', 'Date1_1_19': 'Date1_1_19', 'Date1_1_20': 'Date1_1_20', 'Date1_1_21': 'Date1_1_21', 'Date1_1_22': 'Date1_1_22', 'Date1_1_23': 'Date1_1_23', 'Date1_1_24': 'Date1_1_24', 'Date1_1_25': 'Date1_1_25', 'Date1_1_26': 'Date1_1_26', 'Date1_1_27': 'Date1_1_27', 'Date1_1_28': 'Date1_1_28', 'Date1_1_29': 'Date1_1_29', 'Date1_1_30': 'Date1_1_30', 'Date1_1_31': 'Date1_1_31', 'Date1_1_32': 'Date1_1_32', 'Date1_1_33': 'Date1_1_33', 'Date1_1_34': 'Date1_1_34', 'Date1_1_35': 'Date1_1_35', 'Date1_1_36': 'Date1_1_36', 'Date1_1_37': 'Date1_1_37', 'Date1_1_38': 'Date1_1_38', 'Date1_1_39': 'Date1_1_39', 'Date1_1_40': 'Date1_1_40', 'Date1_1_41': 'Date1_1_41', 'Date1_1_42': 'Date1_1_42', 'RUV85_89': 'RUV85_89', 'RUV90_94': 'RUV90_94', 'RUV95_99': 'RUV95_99', 'RUV00_04': 'RUV00_04', 'RUV05_08': 'RUV05_08', 'RUV_Total': 'RUV_Total', });
lyr_RUV19951999_5.set('fieldAliases', {'OBJECTID': 'OBJECTID', 'MunNombre': 'MunNombre', 'DANE': 'DANE', 'Shape_Leng': 'Shape_Leng', 'Shape_Area': 'Shape_Area', 'OBJECTID_1': 'OBJECTID_1', 'MunNombr_1': 'MunNombr_1', 'DANE_1': 'DANE_1', 'Date1_1_19': 'Date1_1_19', 'Date1_1_20': 'Date1_1_20', 'Date1_1_21': 'Date1_1_21', 'Date1_1_22': 'Date1_1_22', 'Date1_1_23': 'Date1_1_23', 'Date1_1_24': 'Date1_1_24', 'Date1_1_25': 'Date1_1_25', 'Date1_1_26': 'Date1_1_26', 'Date1_1_27': 'Date1_1_27', 'Date1_1_28': 'Date1_1_28', 'Date1_1_29': 'Date1_1_29', 'Date1_1_30': 'Date1_1_30', 'Date1_1_31': 'Date1_1_31', 'Date1_1_32': 'Date1_1_32', 'Date1_1_33': 'Date1_1_33', 'Date1_1_34': 'Date1_1_34', 'Date1_1_35': 'Date1_1_35', 'Date1_1_36': 'Date1_1_36', 'Date1_1_37': 'Date1_1_37', 'Date1_1_38': 'Date1_1_38', 'Date1_1_39': 'Date1_1_39', 'Date1_1_40': 'Date1_1_40', 'Date1_1_41': 'Date1_1_41', 'Date1_1_42': 'Date1_1_42', 'RUV85_89': 'RUV85_89', 'RUV90_94': 'RUV90_94', 'RUV95_99': 'RUV95_99', 'RUV00_04': 'RUV00_04', 'RUV05_08': 'RUV05_08', 'RUV_Total': 'RUV_Total', });
lyr_RUV19901994_6.set('fieldAliases', {'OBJECTID': 'OBJECTID', 'MunNombre': 'MunNombre', 'DANE': 'DANE', 'Shape_Leng': 'Shape_Leng', 'Shape_Area': 'Shape_Area', 'OBJECTID_1': 'OBJECTID_1', 'MunNombr_1': 'MunNombr_1', 'DANE_1': 'DANE_1', 'Date1_1_19': 'Date1_1_19', 'Date1_1_20': 'Date1_1_20', 'Date1_1_21': 'Date1_1_21', 'Date1_1_22': 'Date1_1_22', 'Date1_1_23': 'Date1_1_23', 'Date1_1_24': 'Date1_1_24', 'Date1_1_25': 'Date1_1_25', 'Date1_1_26': 'Date1_1_26', 'Date1_1_27': 'Date1_1_27', 'Date1_1_28': 'Date1_1_28', 'Date1_1_29': 'Date1_1_29', 'Date1_1_30': 'Date1_1_30', 'Date1_1_31': 'Date1_1_31', 'Date1_1_32': 'Date1_1_32', 'Date1_1_33': 'Date1_1_33', 'Date1_1_34': 'Date1_1_34', 'Date1_1_35': 'Date1_1_35', 'Date1_1_36': 'Date1_1_36', 'Date1_1_37': 'Date1_1_37', 'Date1_1_38': 'Date1_1_38', 'Date1_1_39': 'Date1_1_39', 'Date1_1_40': 'Date1_1_40', 'Date1_1_41': 'Date1_1_41', 'Date1_1_42': 'Date1_1_42', 'RUV85_89': 'RUV85_89', 'RUV90_94': 'RUV90_94', 'RUV95_99': 'RUV95_99', 'RUV00_04': 'RUV00_04', 'RUV05_08': 'RUV05_08', 'RUV_Total': 'RUV_Total', });
lyr_RUV19851989_7.set('fieldAliases', {'OBJECTID': 'OBJECTID', 'MunNombre': 'MunNombre', 'DANE': 'DANE', 'Shape_Leng': 'Shape_Leng', 'Shape_Area': 'Shape_Area', 'OBJECTID_1': 'OBJECTID_1', 'MunNombr_1': 'MunNombr_1', 'DANE_1': 'DANE_1', 'Date1_1_19': 'Date1_1_19', 'Date1_1_20': 'Date1_1_20', 'Date1_1_21': 'Date1_1_21', 'Date1_1_22': 'Date1_1_22', 'Date1_1_23': 'Date1_1_23', 'Date1_1_24': 'Date1_1_24', 'Date1_1_25': 'Date1_1_25', 'Date1_1_26': 'Date1_1_26', 'Date1_1_27': 'Date1_1_27', 'Date1_1_28': 'Date1_1_28', 'Date1_1_29': 'Date1_1_29', 'Date1_1_30': 'Date1_1_30', 'Date1_1_31': 'Date1_1_31', 'Date1_1_32': 'Date1_1_32', 'Date1_1_33': 'Date1_1_33', 'Date1_1_34': 'Date1_1_34', 'Date1_1_35': 'Date1_1_35', 'Date1_1_36': 'Date1_1_36', 'Date1_1_37': 'Date1_1_37', 'Date1_1_38': 'Date1_1_38', 'Date1_1_39': 'Date1_1_39', 'Date1_1_40': 'Date1_1_40', 'Date1_1_41': 'Date1_1_41', 'Date1_1_42': 'Date1_1_42', 'RUV85_89': 'RUV85_89', 'RUV90_94': 'RUV90_94', 'RUV95_99': 'RUV95_99', 'RUV00_04': 'RUV00_04', 'RUV05_08': 'RUV05_08', 'RUV_Total': 'RUV_Total', });
lyr_IncursionesFARC_8.set('fieldAliases', {'NATO_ACTO': 'NATO_ACTO', 'FUENTE': 'FUENTE', 'FECHA_DE_F': 'FECHA_DE_F', 'NOMBRE_DEL': 'NOMBRE_DEL', 'FECHA_DE_I': 'FECHA_DE_I', 'AÑO_INCUR': 'AÑO_INCUR', 'DEPARTAMEN': 'DEPARTAMEN', 'Cod_DEPTO': 'Cod_DEPTO', 'MUNICIPIO': 'MUNICIPIO', 'DIVIPOLA_M': 'DIVIPOLA_M', 'DANE': 'DANE', 'DETALLE_DE': 'DETALLE_DE', 'ACTOR_ARMA': 'ACTOR_ARMA', 'UNIDAD': 'UNIDAD', 'DETALLER_P': 'DETALLER_P', 'SEGUNDO_AC': 'SEGUNDO_AC', 'UNIDAD_SEG': 'UNIDAD_SEG', 'DETALLE_SE': 'DETALLE_SE', 'JUICIO_GUE': 'JUICIO_GUE', 'FUSILAMIEN': 'FUSILAMIEN', 'ENFRENTAMI': 'ENFRENTAMI', 'CARÁCTER_': 'CARÁCTER_', 'CONVOCATOR': 'CONVOCATOR', 'BAJAS_GUER': 'BAJAS_GUER', 'NÚMERO_DE': 'NÚMERO_DE', 'HORAS_DE_I': 'HORAS_DE_I', 'MINUTOS_DE': 'MINUTOS_DE', 'TIPO_DE_IN': 'TIPO_DE_IN', 'NATO_ACTO_': 'NATO_ACTO_', 'ACTITUD_CI': 'ACTITUD_CI', 'OBSERVACIO': 'OBSERVACIO', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', 'date2': 'date2', });
lyr_1a4aConferencia_9.set('fieldAliases', {'objectid': 'objectid', 'ccnct': 'ccnct', 'conf_1_4': 'conf_1_4', 'CON14': 'CON14', 'CON1_4': 'CON1_4', 'conf_5_6': 'conf_5_6', 'CONF5_6': 'CONF5_6', 'conf_7': 'conf_7', 'conf_8': 'conf_8', 'conf_9': 'conf_9', 'conf_10': 'conf_10', 'dpto_cnmbr': 'dpto_cnmbr', 'mpio_cnmbr': 'mpio_cnmbr', 'CONF7': 'CONF7', 'CONF8': 'CONF8', 'CONF9': 'CONF9', 'CONF10': 'CONF10', 'Confe7': 'Confe7', 'Shape__Are': 'Shape__Are', 'Shape__Len': 'Shape__Len', });
lyr_5a6aConferencia_10.set('fieldAliases', {'objectid': 'objectid', 'ccnct': 'ccnct', 'conf_1_4': 'conf_1_4', 'COON14': 'COON14', 'CON14': 'CON14', 'conf_5_6': 'conf_5_6', 'CONF5_6': 'CONF5_6', 'CONF56': 'CONF56', 'conf_7': 'conf_7', 'CONF7': 'CONF7', 'CONN7': 'CONN7', 'conf_8': 'conf_8', 'conf_9': 'conf_9', 'conf_10': 'conf_10', 'dpto_cnmbr': 'dpto_cnmbr', 'mpio_cnmbr': 'mpio_cnmbr', 'CONF8': 'CONF8', 'CONF9': 'CONF9', 'CONF10': 'CONF10', 'Shape__Are': 'Shape__Are', 'Shape__Len': 'Shape__Len', });
lyr_7aConferencia_11.set('fieldAliases', {'objectid': 'objectid', 'ccnct': 'ccnct', 'conf_1_4': 'conf_1_4', 'conf_5_6': 'conf_5_6', 'CONF5_6': 'CONF5_6', 'conf_7': 'conf_7', 'CONF7': 'CONF7', 'Confe7': 'Confe7', 'conf_8': 'conf_8', 'conf_9': 'conf_9', 'conf_10': 'conf_10', 'dpto_cnmbr': 'dpto_cnmbr', 'mpio_cnmbr': 'mpio_cnmbr', 'CONF8': 'CONF8', 'CONF9': 'CONF9', 'CONF10': 'CONF10', 'CON14': 'CON14', 'CON1_4': 'CON1_4', 'Shape__Are': 'Shape__Are', 'Shape__Len': 'Shape__Len', });
lyr_8aConferencia_12.set('fieldAliases', {'objectid': 'objectid', 'ccnct': 'ccnct', 'conf_1_4': 'conf_1_4', 'conf_5_6': 'conf_5_6', 'CONF5_6': 'CONF5_6', 'conf_7': 'conf_7', 'conf_8': 'conf_8', 'CONF8': 'CONF8', 'COF8': 'COF8', 'conf_9': 'conf_9', 'conf_10': 'conf_10', 'dpto_cnmbr': 'dpto_cnmbr', 'mpio_cnmbr': 'mpio_cnmbr', 'CONF7': 'CONF7', 'CONF9': 'CONF9', 'CONF10': 'CONF10', 'CON14': 'CON14', 'CON1_4': 'CON1_4', 'Confe7': 'Confe7', 'Shape__Are': 'Shape__Are', 'Shape__Len': 'Shape__Len', });
lyr_9aConferencia_13.set('fieldAliases', {'objectid': 'objectid', 'ccnct': 'ccnct', 'conf_1_4': 'conf_1_4', 'conf_5_6': 'conf_5_6', 'CONF5_6': 'CONF5_6', 'conf_7': 'conf_7', 'conf_8': 'conf_8', 'conf_9': 'conf_9', 'CONF9': 'CONF9', 'CON9': 'CON9', 'conf_10': 'conf_10', 'dpto_cnmbr': 'dpto_cnmbr', 'mpio_cnmbr': 'mpio_cnmbr', 'CONF7': 'CONF7', 'CONF8': 'CONF8', 'CONF10': 'CONF10', 'CON14': 'CON14', 'CON1_4': 'CON1_4', 'Confe7': 'Confe7', 'COF8': 'COF8', 'Shape__Are': 'Shape__Are', 'Shape__Len': 'Shape__Len', });
lyr_10aConferencia_14.set('fieldAliases', {'objectid': 'objectid', 'ccnct': 'ccnct', 'conf_1_4': 'conf_1_4', 'conf_5_6': 'conf_5_6', 'CONF5_6': 'CONF5_6', 'conf_7': 'conf_7', 'conf_8': 'conf_8', 'conf_9': 'conf_9', 'conf_10': 'conf_10', 'CONF10': 'CONF10', 'c10': 'c10', 'dpto_cnmbr': 'dpto_cnmbr', 'mpio_cnmbr': 'mpio_cnmbr', 'CONF7': 'CONF7', 'CONF8': 'CONF8', 'CONF9': 'CONF9', 'CON14': 'CON14', 'CON1_4': 'CON1_4', 'Confe7': 'Confe7', 'COF8': 'COF8', 'CON9': 'CON9', 'Shape__Are': 'Shape__Are', 'Shape__Len': 'Shape__Len', });
lyr_Densidad_de_Coca_National_mod_15.set('fieldAliases', {'OBJECTID': 'OBJECTID', 'ano': 'ano', 'Shape_Leng': 'Shape_Leng', 'Shape_Area': 'Shape_Area', });
lyr_SalidaExilio_16.set('fieldAliases', {'id_entrevi': 'id_entrevi', 'cantidad_p': 'cantidad_p', 'salida_fec': 'salida_fec', 'salida_ani': 'salida_ani', 'salida_lug': 'salida_lug', 'salida_l_1': 'salida_l_1', 'salida_l_2': 'salida_l_2', 'longitud_s': 'longitud_s', 'latitud_sa': 'latitud_sa', 'llegada_fe': 'llegada_fe', 'llegada__1': 'llegada__1', 'llegada__2': 'llegada__2', 'longitud_l': 'longitud_l', 'latitud_ll': 'latitud_ll', 'asentamien': 'asentamien', 'modalidad_': 'modalidad_', 'conteo_vic': 'conteo_vic', });
lyr_RUVTotal_2.set('fieldImages', {'OBJECTID': 'TextEdit', 'MunNombre': 'TextEdit', 'DANE': 'TextEdit', 'Shape_Leng': 'TextEdit', 'Shape_Area': 'TextEdit', 'OBJECTID_1': 'Range', 'MunNombr_1': 'TextEdit', 'DANE_1': 'TextEdit', 'Date1_1_19': 'TextEdit', 'Date1_1_20': 'TextEdit', 'Date1_1_21': 'TextEdit', 'Date1_1_22': 'TextEdit', 'Date1_1_23': 'TextEdit', 'Date1_1_24': 'TextEdit', 'Date1_1_25': 'TextEdit', 'Date1_1_26': 'TextEdit', 'Date1_1_27': 'TextEdit', 'Date1_1_28': 'TextEdit', 'Date1_1_29': 'TextEdit', 'Date1_1_30': 'TextEdit', 'Date1_1_31': 'TextEdit', 'Date1_1_32': 'TextEdit', 'Date1_1_33': 'TextEdit', 'Date1_1_34': 'TextEdit', 'Date1_1_35': 'TextEdit', 'Date1_1_36': 'TextEdit', 'Date1_1_37': 'TextEdit', 'Date1_1_38': 'TextEdit', 'Date1_1_39': 'TextEdit', 'Date1_1_40': 'TextEdit', 'Date1_1_41': 'TextEdit', 'Date1_1_42': 'TextEdit', 'RUV85_89': 'TextEdit', 'RUV90_94': 'TextEdit', 'RUV95_99': 'TextEdit', 'RUV00_04': 'TextEdit', 'RUV05_08': 'TextEdit', 'RUV_Total': 'TextEdit', });
lyr_RUV20052008_3.set('fieldImages', {'OBJECTID': 'TextEdit', 'MunNombre': 'TextEdit', 'DANE': 'TextEdit', 'Shape_Leng': 'TextEdit', 'Shape_Area': 'TextEdit', 'OBJECTID_1': 'Range', 'MunNombr_1': 'TextEdit', 'DANE_1': 'TextEdit', 'Date1_1_19': 'TextEdit', 'Date1_1_20': 'TextEdit', 'Date1_1_21': 'TextEdit', 'Date1_1_22': 'TextEdit', 'Date1_1_23': 'TextEdit', 'Date1_1_24': 'TextEdit', 'Date1_1_25': 'TextEdit', 'Date1_1_26': 'TextEdit', 'Date1_1_27': 'TextEdit', 'Date1_1_28': 'TextEdit', 'Date1_1_29': 'TextEdit', 'Date1_1_30': 'TextEdit', 'Date1_1_31': 'TextEdit', 'Date1_1_32': 'TextEdit', 'Date1_1_33': 'TextEdit', 'Date1_1_34': 'TextEdit', 'Date1_1_35': 'TextEdit', 'Date1_1_36': 'TextEdit', 'Date1_1_37': 'TextEdit', 'Date1_1_38': 'TextEdit', 'Date1_1_39': 'TextEdit', 'Date1_1_40': 'TextEdit', 'Date1_1_41': 'TextEdit', 'Date1_1_42': 'TextEdit', 'RUV85_89': 'TextEdit', 'RUV90_94': 'TextEdit', 'RUV95_99': 'TextEdit', 'RUV00_04': 'TextEdit', 'RUV05_08': 'TextEdit', 'RUV_Total': '', });
lyr_RUV20002004_4.set('fieldImages', {'OBJECTID': 'TextEdit', 'MunNombre': 'TextEdit', 'DANE': 'TextEdit', 'Shape_Leng': 'TextEdit', 'Shape_Area': 'TextEdit', 'OBJECTID_1': 'Range', 'MunNombr_1': 'TextEdit', 'DANE_1': 'TextEdit', 'Date1_1_19': 'TextEdit', 'Date1_1_20': 'TextEdit', 'Date1_1_21': 'TextEdit', 'Date1_1_22': 'TextEdit', 'Date1_1_23': 'TextEdit', 'Date1_1_24': 'TextEdit', 'Date1_1_25': 'TextEdit', 'Date1_1_26': 'TextEdit', 'Date1_1_27': 'TextEdit', 'Date1_1_28': 'TextEdit', 'Date1_1_29': 'TextEdit', 'Date1_1_30': 'TextEdit', 'Date1_1_31': 'TextEdit', 'Date1_1_32': 'TextEdit', 'Date1_1_33': 'TextEdit', 'Date1_1_34': 'TextEdit', 'Date1_1_35': 'TextEdit', 'Date1_1_36': 'TextEdit', 'Date1_1_37': 'TextEdit', 'Date1_1_38': 'TextEdit', 'Date1_1_39': 'TextEdit', 'Date1_1_40': 'TextEdit', 'Date1_1_41': 'TextEdit', 'Date1_1_42': 'TextEdit', 'RUV85_89': 'TextEdit', 'RUV90_94': 'TextEdit', 'RUV95_99': 'TextEdit', 'RUV00_04': 'TextEdit', 'RUV05_08': 'TextEdit', 'RUV_Total': '', });
lyr_RUV19951999_5.set('fieldImages', {'OBJECTID': 'TextEdit', 'MunNombre': 'TextEdit', 'DANE': 'TextEdit', 'Shape_Leng': 'TextEdit', 'Shape_Area': 'TextEdit', 'OBJECTID_1': 'Range', 'MunNombr_1': 'TextEdit', 'DANE_1': 'TextEdit', 'Date1_1_19': 'TextEdit', 'Date1_1_20': 'TextEdit', 'Date1_1_21': 'TextEdit', 'Date1_1_22': 'TextEdit', 'Date1_1_23': 'TextEdit', 'Date1_1_24': 'TextEdit', 'Date1_1_25': 'TextEdit', 'Date1_1_26': 'TextEdit', 'Date1_1_27': 'TextEdit', 'Date1_1_28': 'TextEdit', 'Date1_1_29': 'TextEdit', 'Date1_1_30': 'TextEdit', 'Date1_1_31': 'TextEdit', 'Date1_1_32': 'TextEdit', 'Date1_1_33': 'TextEdit', 'Date1_1_34': 'TextEdit', 'Date1_1_35': 'TextEdit', 'Date1_1_36': 'TextEdit', 'Date1_1_37': 'TextEdit', 'Date1_1_38': 'TextEdit', 'Date1_1_39': 'TextEdit', 'Date1_1_40': 'TextEdit', 'Date1_1_41': 'TextEdit', 'Date1_1_42': 'TextEdit', 'RUV85_89': 'TextEdit', 'RUV90_94': 'TextEdit', 'RUV95_99': 'TextEdit', 'RUV00_04': 'TextEdit', 'RUV05_08': 'TextEdit', 'RUV_Total': '', });
lyr_RUV19901994_6.set('fieldImages', {'OBJECTID': 'TextEdit', 'MunNombre': 'TextEdit', 'DANE': 'TextEdit', 'Shape_Leng': 'TextEdit', 'Shape_Area': 'TextEdit', 'OBJECTID_1': 'Range', 'MunNombr_1': 'TextEdit', 'DANE_1': 'TextEdit', 'Date1_1_19': 'TextEdit', 'Date1_1_20': 'TextEdit', 'Date1_1_21': 'TextEdit', 'Date1_1_22': 'TextEdit', 'Date1_1_23': 'TextEdit', 'Date1_1_24': 'TextEdit', 'Date1_1_25': 'TextEdit', 'Date1_1_26': 'TextEdit', 'Date1_1_27': 'TextEdit', 'Date1_1_28': 'TextEdit', 'Date1_1_29': 'TextEdit', 'Date1_1_30': 'TextEdit', 'Date1_1_31': 'TextEdit', 'Date1_1_32': 'TextEdit', 'Date1_1_33': 'TextEdit', 'Date1_1_34': 'TextEdit', 'Date1_1_35': 'TextEdit', 'Date1_1_36': 'TextEdit', 'Date1_1_37': 'TextEdit', 'Date1_1_38': 'TextEdit', 'Date1_1_39': 'TextEdit', 'Date1_1_40': 'TextEdit', 'Date1_1_41': 'TextEdit', 'Date1_1_42': 'TextEdit', 'RUV85_89': 'TextEdit', 'RUV90_94': 'TextEdit', 'RUV95_99': 'TextEdit', 'RUV00_04': 'TextEdit', 'RUV05_08': 'TextEdit', 'RUV_Total': '', });
lyr_RUV19851989_7.set('fieldImages', {'OBJECTID': 'TextEdit', 'MunNombre': 'TextEdit', 'DANE': 'TextEdit', 'Shape_Leng': 'TextEdit', 'Shape_Area': 'TextEdit', 'OBJECTID_1': 'Range', 'MunNombr_1': 'TextEdit', 'DANE_1': 'TextEdit', 'Date1_1_19': 'TextEdit', 'Date1_1_20': 'TextEdit', 'Date1_1_21': 'TextEdit', 'Date1_1_22': 'TextEdit', 'Date1_1_23': 'TextEdit', 'Date1_1_24': 'TextEdit', 'Date1_1_25': 'TextEdit', 'Date1_1_26': 'TextEdit', 'Date1_1_27': 'TextEdit', 'Date1_1_28': 'TextEdit', 'Date1_1_29': 'TextEdit', 'Date1_1_30': 'TextEdit', 'Date1_1_31': 'TextEdit', 'Date1_1_32': 'TextEdit', 'Date1_1_33': 'TextEdit', 'Date1_1_34': 'TextEdit', 'Date1_1_35': 'TextEdit', 'Date1_1_36': 'TextEdit', 'Date1_1_37': 'TextEdit', 'Date1_1_38': 'TextEdit', 'Date1_1_39': 'TextEdit', 'Date1_1_40': 'TextEdit', 'Date1_1_41': 'TextEdit', 'Date1_1_42': 'TextEdit', 'RUV85_89': 'TextEdit', 'RUV90_94': 'TextEdit', 'RUV95_99': 'TextEdit', 'RUV00_04': 'TextEdit', 'RUV05_08': 'TextEdit', 'RUV_Total': '', });
lyr_IncursionesFARC_8.set('fieldImages', {'NATO_ACTO': '', 'FUENTE': '', 'FECHA_DE_F': '', 'NOMBRE_DEL': '', 'FECHA_DE_I': '', 'AÑO_INCUR': '', 'DEPARTAMEN': '', 'Cod_DEPTO': '', 'MUNICIPIO': '', 'DIVIPOLA_M': '', 'DANE': '', 'DETALLE_DE': '', 'ACTOR_ARMA': '', 'UNIDAD': '', 'DETALLER_P': '', 'SEGUNDO_AC': '', 'UNIDAD_SEG': '', 'DETALLE_SE': '', 'JUICIO_GUE': '', 'FUSILAMIEN': '', 'ENFRENTAMI': '', 'CARÁCTER_': '', 'CONVOCATOR': '', 'BAJAS_GUER': '', 'NÚMERO_DE': '', 'HORAS_DE_I': '', 'MINUTOS_DE': '', 'TIPO_DE_IN': '', 'NATO_ACTO_': '', 'ACTITUD_CI': '', 'OBSERVACIO': '', 'LATITUD': '', 'LONGITUD': '', 'date2': '', });
lyr_1a4aConferencia_9.set('fieldImages', {'objectid': '', 'ccnct': '', 'conf_1_4': '', 'CON14': '', 'CON1_4': '', 'conf_5_6': '', 'CONF5_6': '', 'conf_7': '', 'conf_8': '', 'conf_9': '', 'conf_10': '', 'dpto_cnmbr': '', 'mpio_cnmbr': '', 'CONF7': '', 'CONF8': '', 'CONF9': '', 'CONF10': '', 'Confe7': '', 'Shape__Are': '', 'Shape__Len': '', });
lyr_5a6aConferencia_10.set('fieldImages', {'objectid': '', 'ccnct': '', 'conf_1_4': '', 'COON14': '', 'CON14': '', 'conf_5_6': '', 'CONF5_6': '', 'CONF56': '', 'conf_7': '', 'CONF7': '', 'CONN7': '', 'conf_8': '', 'conf_9': '', 'conf_10': '', 'dpto_cnmbr': '', 'mpio_cnmbr': '', 'CONF8': '', 'CONF9': '', 'CONF10': '', 'Shape__Are': '', 'Shape__Len': '', });
lyr_7aConferencia_11.set('fieldImages', {'objectid': '', 'ccnct': '', 'conf_1_4': '', 'conf_5_6': '', 'CONF5_6': '', 'conf_7': '', 'CONF7': '', 'Confe7': '', 'conf_8': '', 'conf_9': '', 'conf_10': '', 'dpto_cnmbr': '', 'mpio_cnmbr': '', 'CONF8': '', 'CONF9': '', 'CONF10': '', 'CON14': '', 'CON1_4': '', 'Shape__Are': '', 'Shape__Len': '', });
lyr_8aConferencia_12.set('fieldImages', {'objectid': '', 'ccnct': '', 'conf_1_4': '', 'conf_5_6': '', 'CONF5_6': '', 'conf_7': '', 'conf_8': '', 'CONF8': '', 'COF8': '', 'conf_9': '', 'conf_10': '', 'dpto_cnmbr': '', 'mpio_cnmbr': '', 'CONF7': '', 'CONF9': '', 'CONF10': '', 'CON14': '', 'CON1_4': '', 'Confe7': '', 'Shape__Are': '', 'Shape__Len': '', });
lyr_9aConferencia_13.set('fieldImages', {'objectid': '', 'ccnct': '', 'conf_1_4': '', 'conf_5_6': '', 'CONF5_6': '', 'conf_7': '', 'conf_8': '', 'conf_9': '', 'CONF9': '', 'CON9': '', 'conf_10': '', 'dpto_cnmbr': '', 'mpio_cnmbr': '', 'CONF7': '', 'CONF8': '', 'CONF10': '', 'CON14': '', 'CON1_4': '', 'Confe7': '', 'COF8': '', 'Shape__Are': '', 'Shape__Len': '', });
lyr_10aConferencia_14.set('fieldImages', {'objectid': '', 'ccnct': '', 'conf_1_4': '', 'conf_5_6': '', 'CONF5_6': '', 'conf_7': '', 'conf_8': '', 'conf_9': '', 'conf_10': '', 'CONF10': '', 'c10': '', 'dpto_cnmbr': '', 'mpio_cnmbr': '', 'CONF7': '', 'CONF8': '', 'CONF9': '', 'CON14': '', 'CON1_4': '', 'Confe7': '', 'COF8': '', 'CON9': '', 'Shape__Are': '', 'Shape__Len': '', });
lyr_Densidad_de_Coca_National_mod_15.set('fieldImages', {'OBJECTID': 'TextEdit', 'ano': 'DateTime', 'Shape_Leng': 'TextEdit', 'Shape_Area': 'TextEdit', });
lyr_SalidaExilio_16.set('fieldImages', {'id_entrevi': 'TextEdit', 'cantidad_p': 'TextEdit', 'salida_fec': 'DateTime', 'salida_ani': 'TextEdit', 'salida_lug': 'TextEdit', 'salida_l_1': 'TextEdit', 'salida_l_2': 'TextEdit', 'longitud_s': 'TextEdit', 'latitud_sa': 'TextEdit', 'llegada_fe': 'DateTime', 'llegada__1': 'TextEdit', 'llegada__2': 'TextEdit', 'longitud_l': 'TextEdit', 'latitud_ll': 'TextEdit', 'asentamien': 'TextEdit', 'modalidad_': 'TextEdit', 'conteo_vic': 'TextEdit', });
lyr_RUVTotal_2.set('fieldLabels', {});
lyr_RUV20052008_3.set('fieldLabels', {});
lyr_RUV20002004_4.set('fieldLabels', {});
lyr_RUV19951999_5.set('fieldLabels', {});
lyr_RUV19901994_6.set('fieldLabels', {});
lyr_RUV19851989_7.set('fieldLabels', {});
lyr_IncursionesFARC_8.set('fieldLabels', {});
lyr_1a4aConferencia_9.set('fieldLabels', {});
lyr_5a6aConferencia_10.set('fieldLabels', {});
lyr_7aConferencia_11.set('fieldLabels', {});
lyr_8aConferencia_12.set('fieldLabels', {});
lyr_9aConferencia_13.set('fieldLabels', {});
lyr_10aConferencia_14.set('fieldLabels', {});
lyr_Densidad_de_Coca_National_mod_15.set('fieldLabels', {'OBJECTID': 'no label', 'ano': 'no label', 'Shape_Leng': 'no label', 'Shape_Area': 'no label', });
lyr_SalidaExilio_16.set('fieldLabels', {'id_entrevi': 'no label', 'cantidad_p': 'no label', 'salida_fec': 'no label', 'salida_ani': 'no label', 'salida_lug': 'no label', 'salida_l_1': 'no label', 'salida_l_2': 'no label', 'longitud_s': 'no label', 'latitud_sa': 'no label', 'llegada_fe': 'no label', 'llegada__1': 'no label', 'llegada__2': 'no label', 'longitud_l': 'no label', 'latitud_ll': 'no label', 'asentamien': 'no label', 'modalidad_': 'no label', 'conteo_vic': 'no label', });
lyr_SalidaExilio_16.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});